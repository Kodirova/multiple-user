from django.conf.urls.static import static
from django.urls import path

from app import views
from app.views import LessonCreateView, RegistrationView, ProfileView
from registration import settings
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('register/', RegistrationView.as_view(), name='register'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('accounts/login/', auth_views.LoginView.as_view(), name='login'),
    path('accounts/logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('lesson/create', LessonCreateView.as_view(), name='lesson-create')
    ]


if settings.DEBUG:
   urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
