from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db import models

# Create your models here.
from django.urls import reverse

from registration import settings


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    class UserType(models.TextChoices):
        STUDENT = "STUDENT", "Student"
        STAFF = "STAFF", "Staff"
        ADMIN = "ADMIN", "Administrator"

    class UserStatus(models.TextChoices):
        CREATED = "CREATED", "Created"
        VERIFIED = "VERIFIED", "Verified"
        PASS_RESET = "PASS_RESET", "Password reset"

    full_name = models.CharField(max_length=255)
    email = models.EmailField('email address', unique=True)
    phone_number = models.CharField(
        max_length=12,
        unique=True,
        error_messages={"unique": "phone_number already exists"},
    )
    user_type = models.CharField(choices=UserType.choices, max_length=50)
    registered_at = models.DateField(auto_now_add=True)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ["user_type"]
    objects = UserManager()

    status = models.CharField(
        choices=UserStatus.choices, max_length=19, default="CREATED"
    )

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __str__(self):
        return f"{self.email} - {self.full_name}"



class Staff(models.Model):
    full_name = models.CharField(max_length=256)
    phone_number = models.CharField(max_length=9)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)


class Lesson(models.Model):
    number = models.IntegerField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    topic = models.CharField(max_length=256)
    agenda = models.CharField(max_length=256)




    def __str__(self):
        return self.topic





class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    full_name = models.CharField(max_length=256)
    age = models.IntegerField()
    has_laptop = models.BooleanField()
    phone_number = models.CharField(max_length=256)
    status = models.BooleanField(default=False)
    status_in_progress = models.BooleanField(default=True)